package de.itechbs14.springpresentation.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

  private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);


  @Bean
  CommandLineRunner initDatabaseStudents(StudentRepository repository) {

    return args -> {
      log.info("Preloading " + repository.save(new Student("Max Mustermann", 18)));
      log.info("Preloading " + repository.save(new Student("Erika Mustermann", 69)));
    };
  }

  @Bean
  CommandLineRunner initDatabaseVegetables(VegetableRepository repository) {

    return args -> {
      log.info("Preloading " + repository.save(new Vegetable("Apple", 0.12)));
      log.info("Preloading " + repository.save(new Vegetable("Wine", 12.99)));
    };
  }

/**
 @Bean CommandLineRunner updateDatabaseVegetables(VegetableRepository repository) {

 return args -> {
 Vegetable vegetable = repository.findByName("Apple");
 vegetable.setPrice(0.15);
 log.info("Updating " + repository.save(vegetable));
 };
 }
 **/

}