package de.itechbs14.springpresentation.entity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VegetableRepository extends JpaRepository<Vegetable, Long> {

  Vegetable findByName(String name);

}
