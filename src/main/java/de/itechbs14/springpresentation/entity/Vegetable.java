package de.itechbs14.springpresentation.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Vegetable {


  @GeneratedValue
  @Id
  private Long id;
  @Column(unique = true)
  private String name;
  private Double price;
  public Vegetable(String name, Double price) {
    this.name = name;
    this.price = price;
  }

}
